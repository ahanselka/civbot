package main

import (
	"database/sql"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
	_ "github.com/lib/pq"
)

var (
	token           string
	channelID       string
	db              *sql.DB
	bot             *discordgo.Session
	gameTurnCounter map[string]int
)

// WebhookMessage is the data received from a webhook call
type WebhookMessage struct {
	GameName   string `json:"value1"`
	PlayerName string `json:"value2"`
	TurnNumber string `json:"value3"`
}

func init() {

	flag.StringVar(&token, "t", os.Getenv("DISCORD_TOKEN"), "Discord Bot Token")
	flag.StringVar(&channelID, "c", os.Getenv("DISCORD_CHANNEL_ID"), "Discord Channel ID")
	flag.Parse()
	gameTurnCounter = make(map[string]int)
}

func main() {
	var err error
	flag.Parse()
	pgUser := os.Getenv("POSTGRES_USER")
	pgPass := os.Getenv("POSTGRES_PASS")
	pgDB := os.Getenv("POSTGRES_DB")
	pgHost := os.Getenv("POSTGRES_HOST")
	pgSSL := os.Getenv("POSTGRES_SSL")

	psqlInfo := fmt.Sprintf("host=%s user=%s password=%s database=%s sslmode=%s",
		pgHost, pgUser, pgPass, pgDB, pgSSL)
	db, err = sql.Open("postgres", psqlInfo)
	if err != nil {
		panic(err)
	}

	defer db.Close()

	bot, err = discordgo.New("Bot " + token)
	if err != nil {
		fmt.Println("error creating Discord session,", err)
		return
	}

	bot.AddHandler(messageCreate)

	err = bot.Open()
	if err != nil {
		fmt.Println("error opening connection,", err)
		return
	}

	httpServer()
}

// Discord Stuff
func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	if m.Author.ID == s.State.User.ID {
		return
	}

	var param = strings.SplitN(m.Content, " ", 2)
	if !strings.HasPrefix(param[0], "~") || strings.HasPrefix(param[0], "~~") {
		return
	}

	c := m.ChannelID
	u := m.Author.ID

	switch param[0] {
	case "~setname":
		if len(param) <= 1 {
			s.ChannelMessageSend(m.ChannelID, "`~setname` requires at least one argument.")
			return
		}

		playerName := param[1]
		err := updatePlayer(u, playerName)
		if err != nil {
			s.ChannelMessageSend(m.ChannelID, "Apologies, but that player name is already taken.")
			log.Fatal(err)
			return
		}
		s.ChannelMessageSend(m.ChannelID, "Your player name has been updated.")
	case "~addgame":
		if len(param) <= 1 {
			s.ChannelMessageSend(m.ChannelID, "`~addgame` requires at least one argument.")
			return
		}

		gameName := param[1]
		err := updateGame(c, gameName)
		if err != nil {
			s.ChannelMessageSend(m.ChannelID, "Oops, something went awry.")
			log.Fatal(err)
			return
		}
		s.ChannelMessageSend(m.ChannelID, "The game has been added to this channel.")
	default:
		return
	}
}

func sendMessage(res WebhookMessage) {

	message := generateMessage(res)
	gameChannelID, ok := getChannelIDFromGame(res.GameName)

	if !ok {
		bot.ChannelMessageSend(channelID, message)
	} else {
		bot.ChannelMessageSend(gameChannelID, message)
	}
	gameTurnCounter[res.GameName], _ = strconv.Atoi(res.TurnNumber)
}

func generateMessage(res WebhookMessage) string {
	var message string
	discordID, ok := getDiscordIDFromUser(res.PlayerName)
	if !ok {
		message = fmt.Sprintf("Hey %s, it is your turn in game '%s' on turn number %s.", res.PlayerName, res.GameName, res.TurnNumber)
	} else {
		message = fmt.Sprintf("Hey <@%s>, it is your turn in game '%s' on turn number %s.", discordID, res.GameName, res.TurnNumber)
	}

	return message
}

// Player Stuff

func getUserFromDiscordID(u string) (string, bool) {
	sqlStatement := `SELECT playername, discordid FROM users WHERE discordid=$1;`

	var playerName string
	var discordID string

	row := db.QueryRow(sqlStatement, u)
	switch err := row.Scan(&playerName, &discordID); err {
	case sql.ErrNoRows:
		return "", false
	case nil:
		return playerName, true
	default:
		panic(err)
	}
}

func getDiscordIDFromUser(u string) (string, bool) {
	sqlStatement := `SELECT playername, discordid FROM users WHERE playername=$1;`

	var playerName string
	var discordID string

	row := db.QueryRow(sqlStatement, u)
	switch err := row.Scan(&playerName, &discordID); err {
	case sql.ErrNoRows:
		return "", false
	case nil:
		return discordID, true
	default:
		panic(err)
	}
}

func updatePlayer(u string, playerName string) error {
	_, ok := getUserFromDiscordID(u)
	var err error
	if ok {
		db.Exec("UPDATE users SET playername = $1 WHERE discordid = $2", playerName, u)
	} else {
		_, err = db.Exec("INSERT INTO users(playername, discordid) VALUES($1, $2)", playerName, u)
		if err != nil {
			return err
		}
	}
	return err
}

// Game Stuff

func getChannelIDFromGame(c string) (string, bool) {
	sqlStatement := `SELECT gamename, channelid FROM games WHERE gamename=$1;`

	var gameName string
	var channelID string

	row := db.QueryRow(sqlStatement, c)
	switch err := row.Scan(&gameName, &channelID); err {
	case sql.ErrNoRows:
		return "", false
	case nil:
		return channelID, true
	default:
		panic(err)
	}
}

func updateGame(c string, gameName string) error {
	_, ok := getChannelIDFromGame(gameName)
	var err error
	if ok {
		_, err = db.Exec("UPDATE games SET channelid = $2 WHERE gamename = $1", gameName, c)
	} else {
		_, err = db.Exec("INSERT INTO games(gamename, channelid) VALUES($1, $2)", gameName, c)
	}
	return err
}

// HTTP Stuff

func httpServer() {
	logger := log.New(os.Stdout, "http: ", log.LstdFlags)
	logger.Println("Server is starting...")
	listenAddress := ":8080"
	if os.Getenv("HTTP_PORT") != "" {
		listenAddress = ":" + os.Getenv("HTTP_PORT")
	}
	mux := http.NewServeMux()
	mux.HandleFunc("/webhook", webhook)

	log.Printf("listening on: %v", listenAddress)
	log.Fatal(http.ListenAndServe(listenAddress, requestLogger(mux)))
}

func webhook(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	if r.Method != "POST" {
		w.WriteHeader(http.StatusMethodNotAllowed)
		io.WriteString(w, "Only POST requests are supported.")
	}

	decoder := json.NewDecoder(r.Body)

	var res WebhookMessage

	err := decoder.Decode(&res)
	if err != nil {
		panic(err)
	}

	turnNumber, _ := strconv.Atoi(res.TurnNumber)

	w.WriteHeader(http.StatusOK)
	io.WriteString(w, "OK")
	if gameTurnCounter[res.GameName] == turnNumber {
		return
	}

	sendMessage(res)
}

func requestLogger(targetMux http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()

		targetMux.ServeHTTP(w, r)

		// log request by who(IP address)
		requesterIP := r.RemoteAddr

		log.Printf(
			"%s\t\t%s\t\t%s\t\t%v",
			r.Method,
			r.RequestURI,
			requesterIP,
			time.Since(start),
		)
	})
}
