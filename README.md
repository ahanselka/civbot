# CivBot

This bot listens for a webhook from Civilization VI to alert when it is someone's turn.
[This reddit post](https://www.reddit.com/r/civ/comments/aqwwui/play_by_cloud_making_webhook_work_with_discord/)
was very influential in building this. It allows users to set their player name and tie it to their Discord name
so the bot can ping them when it is their turn. If the user has not tied their playername to ID, it will simply
output the playername from the webhook call. From my experience the Civ app will not send a webhook to a URL
with a port number in it (i.e. `http://something.com:8080/webhook`). Additionally, the Mac client does not
seem to send webhooks. I've reported this to 2K but have heard nothing back yet.

## Discord Bot

In order to connect the bot to Discord, you'll need to create a bot on the
[Discord applications page](https://discordapp.com/developers/applications).
Once you create the application, you'll need to create a [bot
user](https://discordapp.com/developers/docs/topics/oauth2#bots). You'll also
need to [add the bot to your
server](https://discordapp.com/developers/docs/topics/oauth2#bot-authorization-flow).
The token for the bot user is what you will set as `DISCORD_TOKEN`.

## Database

The bot requires a postgres database with the following table.

```sql
CREATE TABLE users(
    playername varchar UNIQUE,
    discordid varchar
);
CREATE TABLE games(
    gamename varchar UNIQUE,
    channelid varchar
);
```

## Config

The program requires the following environment variables:

- `DISCORD_TOKEN` - The bot user API token from the Discord developer console.
- `DISCORD_CHANNEL_ID` - The channel ID of the channel the bot should be listening in and sending notices to.
- `POSTGRES_USER` - PostgreSQL username
- `POSTGRES_PASS` - PostgreSQL password
- `POSTGRES_DB` - PostgreSQL database name
- `POSTGRES_HOST` - PostgreSQL hostname
- `POSTGRES_SSL` - PostgreSQL sslmode.

The following environment variables are optional.

- `HTTP_PORT` - Which port to listen on. The listen IP is currently not configurable.
