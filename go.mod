module gitlab.com/ahanselka/civbot

go 1.13

require (
	github.com/DATA-DOG/go-sqlmock v1.3.3
	github.com/bwmarrin/discordgo v0.19.0
	github.com/lib/pq v1.1.1
	golang.org/x/crypto v0.0.0-20190621222207-cc06ce4a13d4 // indirect
	golang.org/x/sys v0.0.0-20190626221950-04f50cda93cb // indirect
)
